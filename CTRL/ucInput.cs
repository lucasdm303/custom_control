﻿using System;
using System.Windows.Forms;

namespace CTRL
{

    public partial class ucInput : System.Windows.Forms.UserControl
    {
        private string textBox;
        private string textButton;
        private string textGroup;

        public event EventHandler ButtonClicked;

        public ucInput()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("Empty");
            }
            else
            {
                if (this.ButtonClicked != null)
                {
                    this.ButtonClicked(this, e);
                }
            }
        }

        public string TextBox
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; Invalidate(); }
        }

        public string TextButton
        {
            get { return button1.Text; }
            set { textBox1.Text = value; Invalidate(); }
        }

        public string TextGroup
        {
            get { return groupBox1.Text; }
            set { textBox1.Text = value; Invalidate(); }
        }
    }
}