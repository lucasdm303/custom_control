﻿namespace UI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucInput1 = new CTRL.ucInput();
            this.SuspendLayout();
            // 
            // ucInput1
            // 
            this.ucInput1.Location = new System.Drawing.Point(12, 12);
            this.ucInput1.Name = "ucInput1";
            this.ucInput1.Size = new System.Drawing.Size(123, 85);
            this.ucInput1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 243);
            this.Controls.Add(this.ucInput1);
            this.Name = "frmMain";
            this.ResumeLayout(false);

        }

        #endregion

        private CTRL.ucInput ucInput1;
    }
}

