﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CTRL;

namespace UI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            ucInput1.ButtonClicked += new EventHandler(UC_ButtonClick);
        }

        protected void UC_ButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show(ucInput1.TextBox);
        }
    }
}
